package cat.dam.rocriba.navegador;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;

public class Detectorconnexio {
    private Context mContext;

    //Constructor
    public Detectorconnexio(Context context){
        this.mContext = context;
    }

    //Comprovem les connexions disponibles
    public boolean teConnexio() {
        final ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            if (Build.VERSION.SDK_INT >= 23) {
                //Versions superiors
                final Network network = connectivityManager.getActiveNetwork();
                if (network != null) {
                    final NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(network);
                    return (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                            networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI));

                } else {
                    //Versions inferiors
                    final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                    if (networkInfo != null) {
                        return (networkInfo.isConnected() && (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE ||
                                networkInfo.getType() == ConnectivityManager.TYPE_WIFI));
                    }

                }
            }

        }
        return false;
    }
}
