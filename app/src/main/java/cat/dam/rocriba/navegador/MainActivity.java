package cat.dam.rocriba.navegador;

import androidx.appcompat.app.AppCompatActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.net.URL;


public class MainActivity extends AppCompatActivity {
   //Inicialitzem variables

    Boolean internetDisponible = false;
    Detectorconnexio detectorConnexio;
    //Creem funcions
    public static boolean isValid(String url)
    {
        try
        {
            new URL(url).toURI();
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
     private WebView webView;
     private ProgressDialog progDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Inicalitzem variables
        TextView tv_principal = (TextView) findViewById(R.id.tv_1);
        EditText et_principal = (EditText) findViewById(R.id.et_1);
        Button buton_princiapl = (Button) findViewById(R.id.btn_1);
        et_principal.setVisibility(View.VISIBLE);

        //En cas de cliclar el boto carrega  i comprovala web
        buton_princiapl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "";
                url =  url +et_principal.getText().toString();
                //Creem detector
                detectorConnexio = new Detectorconnexio(getApplicationContext());

                //Comprovem si tenim connexió a internet
                internetDisponible = detectorConnexio.teConnexio();
                //Comprovem que existeix la url i tenim internet, si és que si, carregem
                if (internetDisponible && isValid(url)) {

                    //Carreguem pagina
                    progDialog = ProgressDialog.show(MainActivity.this, "Carregant la pàgina", "Siusplau esperi....", true);
                    progDialog.setCancelable(true);
                    webView = (WebView) findViewById(R.id.webView);
                    final WebSettings webSettings = webView.getSettings();
                    webSettings.setJavaScriptEnabled(true);
                    webSettings.setBuiltInZoomControls(true);
                    webSettings.setLoadWithOverviewMode(true);
                    webSettings.setUseWideViewPort(true);

                    webView.setWebViewClient(new WebViewClient() {
                        @Override
                        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
                            progDialog.show();
                            webView.loadUrl(url);
                            return true;
                        }

                        @Override
                        public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                                       SslError error) {
                            handler.proceed();
                        }

                        @Override
                        public void onPageFinished(WebView view, final String url) {
                            progDialog.dismiss();
                        }

                    });
                       //Si totes les condicions anteriors es compleixen, mostrem la pàgina web
                        webView.loadUrl(url);

                    buton_princiapl.setVisibility(View.GONE);
                    tv_principal.setVisibility(View.GONE);


                } else {
                    //Com que no tenim internet o la url no està bé, comprovem quina de les dos és i mostrem l'error per el textview

                    if (!internetDisponible){

                        tv_principal.setVisibility(View.VISIBLE);
                        tv_principal.setText("NO hi ha connexió a internet");
                    }

                    if (!isValid(url)){

                        tv_principal.setVisibility(View.VISIBLE);
                        tv_principal.setText("NO existeix la pàgina web");
                    }
                }

            }

        });
        //tv_principal.setVisibility(View.GONE);


        }
}